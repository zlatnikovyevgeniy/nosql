package com.shpp.app;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;
import static com.shpp.app.CompleteTransaction.*;
import static com.shpp.app.DBInteractor.csvWriter;


public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    public static final int NUMBER_OF_PRODUCTS = Integer.parseInt(PropertyReader
            .readProperty("config.properties")
            .getProperty("num_of_messages"));

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        sendStreamOfProducts();
        stopWatch.stop();
        long time = stopWatch.getTime(TimeUnit.SECONDS);
        logger.info("Time: {}. WPS = {}", time, (double) NUMBER_OF_PRODUCTS / time);
        csvWriter.closeFile();
        stopWatch.reset();

        MongoClient mongoClient = MongoClients.create(URL);
        DBInteractor dbInteractor = new DBInteractor(mongoClient);
        stopWatch.start();
        dbInteractor.findFromCollection();
        stopWatch.stop();
        logger.info("Find time - {}", stopWatch.getTime(TimeUnit.MILLISECONDS));
        mongoClient.close();

    }
}