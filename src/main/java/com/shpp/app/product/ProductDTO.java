package com.shpp.app.product;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;

import java.util.StringJoiner;

public class ProductDTO {

    @PositiveOrZero
    long id;
    @Pattern(regexp = "[a-zA-Z]{3,20}")
    String name;
    @Pattern(regexp = "[a-zA-Z]{3,20}")
    String category;

    public ProductDTO() {
    }

    public ProductDTO(String name, String category) {
        this.name = name;
        this.category = category;
    }

    public ProductDTO(long id, String name, String category) {
        this.id = id;
        this.name = name;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ProductDTO.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("category=" + category)
                .toString();
    }
}
