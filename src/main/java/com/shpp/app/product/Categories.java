package com.shpp.app.product;

public enum Categories {
    SPORT(1, "sport"),
    FURNITURE(2, "furniture"),
    HOME_GOODS(3,"home goods"),
    PET_PRODUCTS(4,"pet products"),
    SMARTPHONES(5,"smartphones"),
    LAPTOPS(6,"laptops"),
    APPLIANCES(7,"appliances"),
    GARMENT(8,"garment"),
    SHOES(9,"shoes"),
    HOUSEHOLD_CHEMICALS(10,"household chemicals"),
    TOOLS(11,"tools"),
    AUTO_PRODUCTS(12,"auto products"),
    PRODUCTS(13,"products"),
    ALCOHOLIC_DRINKS(14,"alcoholic drinks"),
    BEAUTY_AND_HEALTH(15,"beauty and health"),
    TOURISM(16,"tourism"),
    STATIONERY(17,"stationery"),
    PHARMACY(18,"pharmacy");

    Categories(long id, String name) {
        this.id = id;
        this.name = name;
    }

    private final long id;
    private final String name;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
