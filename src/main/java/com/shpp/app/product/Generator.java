package com.shpp.app.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class Generator {

    private static final Logger logger = LoggerFactory.getLogger(Generator.class);

    private static int getRandomNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public static ProductDTO generateProduct(){
        return new ProductDTO(generateStringWithLetters(), generateStringWithLetters());
    }

    public static StorageDTO generateStorage(){
        return new StorageDTO(getRandomNumber(0,1_000_000),getRandomNumber(1,30000),getRandomNumber(1,20));
    }

    public static String generateStringWithLetters() {
        int leftLimit = 97;
        int rightLimit = 122;
        int targetStringLength = getRandomNumber(3, 20);
        logger.debug("Create 3 auxiliary variables to generate a string with targetStringLength " +
                "from the small letter a to the small letter z in ASCII table.");
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static String generateCategory(){
        int category = getRandomNumber(1,18);
        for (Categories c : Categories.values()){
            if(c.getId()==category) return c.getName();
        }
        return "undefined";
    }


}
