package com.shpp.app.product;

import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;

public class StorageDTO {
    @PositiveOrZero
    long id;
    @PositiveOrZero
    int quantity;
    @Positive
    long productId;
    @Positive
    long shopId;

    public StorageDTO() {
    }

    public StorageDTO(int quantity, long productId, long shopId) {
        this.quantity = quantity;
        this.productId = productId;
        this.shopId = shopId;
    }

    public StorageDTO(long id, int quantity, long productId, long shopId) {
        this.id = id;
        this.quantity = quantity;
        this.productId = productId;
        this.shopId = shopId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }
}
