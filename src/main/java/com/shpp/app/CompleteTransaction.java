package com.shpp.app;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.shpp.app.DBInteractor.TRANSACTION_SIZE;
import static com.shpp.app.Main.NUMBER_OF_PRODUCTS;
import static com.shpp.app.PropertyReader.readProperty;

public class CompleteTransaction extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(CompleteTransaction.class);
    private static final int NUMBER_OF_THREADS = 10;
    public static final String FILENAME = "connection.properties";
    public static final String URL = readProperty(FILENAME).getProperty("url");
    public static final String DB_NAME = readProperty(FILENAME).getProperty("db_name");

    private static int messageCounter = 0;

    @Override
    public void run() {
        MongoClient mongoClient = MongoClients.create(URL);
        DBInteractor dbInteractor = new DBInteractor(mongoClient);
        dbInteractor.insertIntoProductsCollection();
        messageCounter += TRANSACTION_SIZE;
        logger.info("{} products was sent", messageCounter);
        mongoClient.close();
        System.gc();
    }

    public static void sendStreamOfProducts() {
        int counter = 0;
        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
        while (counter < NUMBER_OF_PRODUCTS) {
            executorService.execute(new CompleteTransaction());
            counter += TRANSACTION_SIZE;
        }
        executorService.shutdown();
        while (!executorService.isTerminated()) {}
    }
}
