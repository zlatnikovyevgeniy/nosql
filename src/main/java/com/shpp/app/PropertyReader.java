package com.shpp.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Properties;

public class PropertyReader {

    private static final Logger logger = LoggerFactory.getLogger(PropertyReader.class);

    public static Properties readProperty(String fileName) {
        Properties configProps = new Properties();
        logger.debug("A new object of Properties is created.");

        logger.debug("Trying to read {}.", fileName);
        try (InputStreamReader isr = new InputStreamReader(new FileInputStream(fileName),
                StandardCharsets.UTF_8)) {
            configProps.load(isr);
        } catch (IOException e) {
            try (InputStreamReader r = new InputStreamReader(Objects.requireNonNull(PropertyReader.class.
                    getClassLoader().getResourceAsStream(fileName)), StandardCharsets.UTF_8)) {
                configProps.load(r);
                logger.debug("Trying to read internal {}", fileName);
            } catch (IOException ex) {
                logger.error("Internal property file doesn't exist!", ex);
                throw new RuntimeException("Can't read properties!");
            }
        }
        logger.debug("Configuration parameters were read from property file {}.", fileName);

        return configProps;
    }
}
