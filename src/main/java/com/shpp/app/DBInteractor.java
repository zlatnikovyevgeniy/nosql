package com.shpp.app;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;
import com.shpp.app.product.Generator;
import com.shpp.app.product.ProductDTO;
import com.shpp.app.product.StorageDTO;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.bson.Document;
import org.bson.json.JsonWriterSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static com.mongodb.client.model.Aggregates.*;
import static com.shpp.app.CompleteTransaction.DB_NAME;


public class DBInteractor {
    private static final Logger logger = LoggerFactory.getLogger(DBInteractor.class);
    public static final int TRANSACTION_SIZE = Integer.parseInt(PropertyReader
            .readProperty("config.properties")
            .getProperty("trn_size"));
    private MongoDatabase db;
    private ValidatorFactory factory;
    private Validator validator;
    private Set<ConstraintViolation<ProductDTO>> violations;
    private Set<ConstraintViolation<StorageDTO>> storage_Violations;
    public static final CSVWriter csvWriter = new CSVWriter();

    public DBInteractor(MongoClient mongoClient) {
        this.db = mongoClient.getDatabase(DB_NAME);
        factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public void insertIntoProductsCollection() {
        MongoCollection collection = db.getCollection("products");
        List<Document> toInsert = new ArrayList<>();

        Stream.generate(Generator::generateProduct)
                .limit(TRANSACTION_SIZE)
                .forEach(productDTO -> {
                    violations = validator.validate(productDTO);
                    if (violations.isEmpty()) {
                        toInsert.add(new Document("name", productDTO.getName())
                                .append("category", productDTO.getCategory()));
                    } else {
                        List<String> list = new ArrayList<>();
                        for (ConstraintViolation<ProductDTO> v : violations) {
                            list.add(v.getMessage());
                        }
                        csvWriter.renderInvalidMessages(productDTO, list);
                    }
                });

        collection.insertMany(toInsert);
    }

    public void insertIntoShopsCollection(Document doc) {
        MongoCollection collection = db.getCollection("shops");
        collection.insertOne(doc);
    }

    public void insertIntoStorageCollection() {
        MongoCollection collection = db.getCollection("storage");
        List<Document> toInsert = new ArrayList<>();

        Stream.generate(Generator::generateStorage)
                .filter(storageDTO -> {
                    storage_Violations = validator.validate(storageDTO);
                    return storage_Violations.isEmpty();
                })
                .limit(TRANSACTION_SIZE)
                .forEach(storageDTO -> {
                    toInsert.add(new Document("quantity", storageDTO.getQuantity())
                            .append("product_id", storageDTO.getProductId())
                            .append("shop_id", storageDTO.getShopId()));
                });

        collection.insertMany(toInsert);
    }

    public void findFromCollection() {
        MongoCollection collection = db.getCollection("products");
        Document shopIdWithMaxQuantity = (Document) collection.aggregate(Arrays.asList(
                        match(Filters.eq("category", System.getProperty("type", "sport"))),
                        lookup("storage", "_id", "product_id", "storage"),
                        unwind("$storage"),
                        group("$storage.shop_id", Accumulators.sum("sum", "$storage.quantity")),
                        sort(Sorts.descending("sum"))
                )
        ).first();
        String id = shopIdWithMaxQuantity.get("_id").toString();
        logger.info(shopIdWithMaxQuantity.toJson(JsonWriterSettings.builder().indent(true).build()));

        MongoCollection shops = db.getCollection("shops");
        Document shopAddress = (Document) shops.find(Filters.eq("_id", Integer.parseInt(id))).first();
        logger.info(shopAddress.toJson(JsonWriterSettings.builder().indent(true).build()));

    }
}
