package com.shpp.app.product;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductValidationTest {
    private static final Logger logger = LoggerFactory.getLogger(ProductValidationTest.class);

    private static Validator validator;

    @BeforeAll
    static void setUp(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void validIdTest(){
        ProductDTO productDTO = new ProductDTO(0,"ryneuiutnnn","sport");
        Set<ConstraintViolation<ProductDTO>> violations = validator.validate(productDTO);
        for(ConstraintViolation<ProductDTO> v:violations) logger.info(v.getMessage());
        assertEquals(0, violations.size());
    }

    @Test
    void invalidIdTest(){
        ProductDTO productDTO = new ProductDTO(-3,"qrfwecer","sport");
        Set<ConstraintViolation<ProductDTO>> violations = validator.validate(productDTO);
        for(ConstraintViolation<ProductDTO> v:violations) logger.info(v.getMessage());
        assertEquals(1, violations.size());
    }

    @Test
    void validNameTest(){
        ProductDTO productDTO = new ProductDTO(0,"qwrtyy","sport");
        Set<ConstraintViolation<ProductDTO>> violations = validator.validate(productDTO);
        for(ConstraintViolation<ProductDTO> v:violations) logger.info(v.getMessage());
        assertEquals(0, violations.size());
    }

    @Test
    void invalidNameWithNumbersTest(){
        ProductDTO productDTO = new ProductDTO(1,"qwrtyy123","sport");
        Set<ConstraintViolation<ProductDTO>> violations = validator.validate(productDTO);
        for(ConstraintViolation<ProductDTO> v:violations) logger.info(v.getMessage());
        assertEquals(1, violations.size());
    }

    @Test
    void invalidBlankNameTest(){
        ProductDTO productDTO = new ProductDTO(1,"","sport");
        Set<ConstraintViolation<ProductDTO>> violations = validator.validate(productDTO);
        for(ConstraintViolation<ProductDTO> v:violations) logger.info(v.getMessage());
        assertEquals(1, violations.size());
    }

    @Test
    void invalidTooLongNameTest(){
        ProductDTO productDTO = new ProductDTO(1,"iuehiowefhqnqecnoeicrtgerhy","sport");
        Set<ConstraintViolation<ProductDTO>> violations = validator.validate(productDTO);
        for(ConstraintViolation<ProductDTO> v:violations) logger.info(v.getMessage());
        assertEquals(1, violations.size());
    }

    @Test
    void validCategoryIdTest(){
        ProductDTO productDTO = new ProductDTO(1,"abyetrumy","sport");
        Set<ConstraintViolation<ProductDTO>> violations = validator.validate(productDTO);
        for(ConstraintViolation<ProductDTO> v:violations) logger.info(v.getMessage());
        assertEquals(0, violations.size());
    }

    @Test
    void invalidCategoryIdTest(){
        ProductDTO productDTO = new ProductDTO(1,"ryneuiutnnn","12345");
        Set<ConstraintViolation<ProductDTO>> violations = validator.validate(productDTO);
        for(ConstraintViolation<ProductDTO> v:violations) logger.info(v.getMessage());
        assertEquals(1, violations.size());
    }

    @Test
    void invalidProductTest(){
        ProductDTO productDTO = new ProductDTO(-1,"1234","12345");
        Set<ConstraintViolation<ProductDTO>> violations = validator.validate(productDTO);
        for(ConstraintViolation<ProductDTO> v:violations) logger.info(v.getMessage());
        assertEquals(3, violations.size());
    }
}
