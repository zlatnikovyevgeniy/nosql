package com.shpp.app.product;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class GeneratorTest {

    @Test
    void generateProductTest() {
        Assertions.assertDoesNotThrow(Generator::generateProduct);
    }

    @Test
    void generateStorageTest(){
        Assertions.assertDoesNotThrow(Generator::generateStorage);
    }

    @Test
    void generateStringWithLettersTest() {
        Assertions.assertDoesNotThrow(Generator::generateStringWithLetters);
    }

    @Test
    void generateCategoryTest(){
        Assertions.assertDoesNotThrow(Generator::generateCategory);
    }

}