package com.shpp.app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static com.shpp.app.PropertyReader.readProperty;

class PropertyReaderTest {

    private static final String CONNECTION_PROPERTY = "connection.properties";
    private static final String CONFIG_PROPERTY = "config.properties";

    @ParameterizedTest
    @MethodSource("provideValuesForReadConnectionPropertyTest")
    void readConnectionPropertyTest(String value, String propertyName) {
        Assertions.assertEquals(value, readProperty(CONNECTION_PROPERTY).getProperty(propertyName));
    }

    private static Stream<Arguments> provideValuesForReadConnectionPropertyTest() {
        return Stream.of(
                Arguments.of("mongodb+srv:reference", "url"),
                Arguments.of("DB_Name", "db_name")
        );
    }

    @Test
    void readPropertyNegativeTest() {
        Assertions.assertThrows(RuntimeException.class, () -> readProperty(null));
    }

    @ParameterizedTest
    @MethodSource("provideValuesForReadConfigPropertyTest")
    void readConfigPropertyTest(String value, String propertyName) {
        Assertions.assertEquals(value, readProperty(CONFIG_PROPERTY).getProperty(propertyName));
    }

    private static Stream<Arguments> provideValuesForReadConfigPropertyTest() {
        return Stream.of(
                Arguments.of("10000", "trn_size"),
                Arguments.of("3000000", "num_of_messages")
        );
    }
}