package com.shpp.app;

import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.*;

@ExtendWith(MockitoExtension.class)
class DBInteractorTest {

    @InjectMocks
    private static DBInteractor dbInteractor;

    @Mock
    private static MongoDatabase db;

    @Mock
    private static MongoCollection products;

    @Mock
    private static MongoCollection shops;

    @Mock
    private AggregateIterable aggregateIterable;

    @Mock
    private FindIterable findIterable;

    @Mock
    private Document findDocTest;

    @BeforeAll
    static void setUp() {
        MongoClient mongoClient = Mockito.mock(MongoClient.class);
        Mockito.when(mongoClient.getDatabase(anyString())).thenReturn(db);
        dbInteractor = new DBInteractor(mongoClient);
    }

    @Test
    void insertIntoProductsCollectionTest() {
        ArgumentCaptor<String> collectionName = ArgumentCaptor.forClass(String.class);
        Mockito.when(db.getCollection(anyString())).thenReturn(products);
        dbInteractor.insertIntoProductsCollection();

        Mockito.verify(db,Mockito.times(1)).getCollection(collectionName.capture());
        Assertions.assertEquals("products",collectionName.getValue());
        Mockito.verify(products,Mockito.times(1)).insertMany(anyList());
    }

    @Test
    void insertIntoShopsCollectionTest() {
        ArgumentCaptor<String> collectionName = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Document> doc = ArgumentCaptor.forClass(Document.class);
        Document document = new Document("_id", 1).append("address", "Bratuslavska 11");
        Mockito.when(db.getCollection(anyString())).thenReturn(products);
        dbInteractor.insertIntoShopsCollection(document);

        Mockito.verify(db,Mockito.times(1)).getCollection(collectionName.capture());
        Assertions.assertEquals("shops",collectionName.getValue());
        Mockito.verify(products,Mockito.times(1)).insertOne(doc.capture());
        Assertions.assertEquals(document,doc.getValue());
    }

    @Test
    void insertIntoStorageCollectionTest() {
        ArgumentCaptor<String> collectionName = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<ArrayList<Document>> list = ArgumentCaptor.forClass(ArrayList.class);
        Mockito.when(db.getCollection(anyString())).thenReturn(products);
        dbInteractor.insertIntoStorageCollection();

        Mockito.verify(db,Mockito.times(1)).getCollection(collectionName.capture());
        Assertions.assertEquals("storage",collectionName.getValue());
        Mockito.verify(products,Mockito.times(1)).insertMany(list.capture());
        Assertions.assertEquals(10000,list.getValue().size());
    }

    @Test
    void findFromCollectionTest() {
        Integer id = 6;
        ArgumentCaptor<String> collectionName = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> fieldName = ArgumentCaptor.forClass(String.class);
        Mockito.when(db.getCollection("products")).thenReturn(products);
        Mockito.when(products.aggregate(anyList())).thenReturn(aggregateIterable);
        Mockito.when(aggregateIterable.first()).thenReturn(findDocTest);
        Mockito.when(findDocTest.get(anyString())).thenReturn(new Integer(id));

        Mockito.when(db.getCollection("shops")).thenReturn(shops);
        Mockito.when(shops.find(Filters.eq("_id", id))).thenReturn(findIterable);
        Mockito.when(findIterable.first()).thenReturn(new Document());
        dbInteractor.findFromCollection();

        Mockito.verify(db,Mockito.times(2)).getCollection(collectionName.capture());
        Assertions.assertEquals("shops",collectionName.getValue());
        Mockito.verify(products,Mockito.times(1)).aggregate(anyList());
        Mockito.verify(aggregateIterable,Mockito.times(1)).first();
        Mockito.verify(findDocTest,Mockito.times(1)).get(fieldName.capture());
        Assertions.assertEquals("_id",fieldName.getValue());

        Mockito.verify(shops,Mockito.times(1)).find(Filters.eq("_id", id));
        Mockito.verify(findIterable,Mockito.times(1)).first();
    }
}